# Copyright (c) 2013, Libracore AG and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe

def execute(filters=None):
	columns, data = [], []
	columns = ["Time", "Temperature", "Humidity"]

	logs = frappe.db.sql("SELECT modified, value, humidity FROM `tabSensor data` ORDER BY modified", as_dict=1)

	for log in logs:
		data.append([log['modified'], log['value'], log['humidity']])

	return columns, data
